//
//  ProgressModel.m
//  BlocksLesson
//
//  Created by Dudarenko Ilya on 19.04.17.
//  Copyright © 2017 DIO. All rights reserved.
//

#import "ProgressModel.h"

@implementation ProgressModel

- (void)doTask {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 0; i <= 100; i++) {
            usleep(10000);
            dispatch_async(dispatch_get_main_queue(), ^{
                float currentProgress = (float)i/100;
                if ([self.delegate respondsToSelector:@selector(didProgress:)]) {
                    [self.delegate didProgress:currentProgress];
                }
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate didFinishTask];
        });
    });
}

- (void)doTaskWithProgressBlock:(PMProgressBlock)progressBlock
                completionBlock:(PMCompletionBlock)completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 0; i <= 100; i++) {
            usleep(10000);
            dispatch_async(dispatch_get_main_queue(), ^{
                float currentProgress = (float)i/100;
                progressBlock(currentProgress);
            });
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock();
        });
    });
}

@end
