//
//  ProgressViewController.m
//  BlocksLesson
//
//  Created by Alexander Dupree on 19/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import "ProgressViewController.h"
#import "ProgressModel.h"

@interface ProgressViewController () <ProgressModelDelegate>

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) ProgressModel *model;
//@property (nonatomic, copy) void (^progressBlock)(float progress);

@end

@implementation ProgressViewController

#pragma mark - Getters

- (ProgressModel *)model {
    if (!_model) {
        _model = [ProgressModel new];
    }
    return _model;
}

#pragma mark - View life cycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.model.delegate = self;
    
//    __weak typeof(self) weakSelf = self;
//    self.progressBlock = ^(float progress){
//        __strong typeof(weakSelf) strongSelf = weakSelf;
//        strongSelf.progressView.progress = progress;
//    };
    
    [self performTimeConsumingTask];
}

#pragma mark -

- (void)performTimeConsumingTask {
    self.progressView.progress = 0;
    
    [self.model doTask];
    
//    [self.model doTaskWithProgressBlock:self.progressBlock
//                                completionBlock:^{
//                                    [self showPopUp];
//                                }];
}

#pragma mark - Popup

- (void)showPopUp {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Progress completed!"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self performTimeConsumingTask];
    }];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - ProgressModelDelegate

- (void)didFinishTask {
    [self showPopUp];
}

- (void)didProgress:(float)progress {
    self.progressView.progress = progress;

}

@end
