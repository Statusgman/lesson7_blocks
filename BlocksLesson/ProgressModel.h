//
//  ProgressModel.h
//  BlocksLesson
//
//  Created by Dudarenko Ilya on 19.04.17.
//  Copyright © 2017 DIO. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProgressModelDelegate <NSObject>

- (void)didFinishTask;

@optional
- (void)didProgress:(float)progress;

@end

typedef void (^PMProgressBlock)(float progress);
typedef void (^PMCompletionBlock)(void);

@interface ProgressModel : NSObject

- (void)doTask;
@property (weak) id<ProgressModelDelegate> delegate;

- (void)doTaskWithProgressBlock:(PMProgressBlock)progressBlock
                completionBlock:(PMCompletionBlock)completionBlock;

@end
