//
//  main.m
//  BlocksLesson
//
//  Created by Alexander Dupree on 19/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
